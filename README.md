# Caesar Cipher

Julius Caesar protected his confidential information by encrypting it usinga cipher shifts each

letter by a number of letters. If the shift takes you past the end of the alphabet, it rotates back to the front of the

alphabet. In the case of a rotation by 3, w,x, y and z would map to z,a,b and c.

~~~
Original alphabet:      abcdefghijklmnopqrstuvwxyz
Alphabet rotated +3:    defghijklmnopqrstuvwxyzabc
~~~

### Example

__s = There's-a-starman-waiting-in-the-sky__

__k=3__

The alphabet is rotated by __3__, matching the mapping above. The encrypted string is

__Wkhuh'v-dvwdupdq-zdlwlwlq-zdlwlqj-lq-wkh-vnb__

__Note:__ The cipher only encrypts letters; symbols, such as -, remain unencrypted.

## Function Description

Complete the caesarCipher function in the editor below.

caesarCipher has the following parameter(s):

- string s: cleartext
- int k: the alphabet rotation factor
### Returns

- string: the encrypted string
### Input Format

The first line contains the integer, __n__, the length of the unencrypted string.
The second line contains the unencrypted string, __s__.The third line contains __k__, the number of letters to rotate the alphabet by.

### Constraints

1 <= n <= 100
0 <= k <= 100

__s__ is a valid ASCII string without any spaces

### Sample Input
~~~
11
middle-Outz
2
~~~
### Sample Output 
~~~
okffng-Qwvb
~~~
### Explanation 
~~~
Original alphabet:      abcdefghijklmnopqrstuvwxyz
Alphabet rotated +2:    cdefghijklmnopqrstuvwxyzab

m -> o
i -> k
d -> f
d -> f
l -> n
e -> g
-    -
O -> Q
u -> w
t -> v
z -> 
~~~